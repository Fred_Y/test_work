function GenerateMatrix(rows, columns){
    var arr = [];
    var k = 0;

    for(var i=0; i<rows; i++){
      arr[i] = [];
      
      for(var j=0; j<columns; j++){
        arr[i][j] = i%2 ? (i+1) * columns-j-1 : k;
        k++;
      }
    }

    return arr;
  }
  
  

  console.log(GenerateMatrix(4,6));
  